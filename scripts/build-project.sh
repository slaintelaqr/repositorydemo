rm -rf bin
mkdir bin
find src -type f -name '*.java' -exec javac -d bin -cp lib/junit3.8.1.jar {} +

testCases=(`find bin/com/laqr/test -type f -name '*.class'`)
for test in "${testCases[@]}"
do
    filename=$(basename "$test")
    parent1="$(basename "$(dirname "$test")")"
    parent2="$(basename "$(dirname "$(dirname "$test")")")"
    parent3="$(basename "$(dirname "$(dirname "$(dirname "$test")")")")"
    file="${filename%.*}"
    java -cp bin:lib/junit3.8.1.jar junit.textui.TestRunner "$parent3.$parent2.$parent1.$file"

    if [ $? -ne 0 ]; then
        exit 1;
    fi
done

cd bin
find com -type f -name '*.class' -exec jar cfe Calculator.jar com.laqr.calculator.CalculatorDriver {} +
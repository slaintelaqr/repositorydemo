package com.laqr.calculator;

/**
 * Calculator Functions
 * 
 * @author laqr
 */
public class CalcFunctions {

	/**
	 * Returns the addition of two numbers
	 * 
	 * @param a
	 * @param b
	 * @return addition of a and b
	 */
	public int add(int a, int b) {
		return a + b;
	}

	/**
	 * Returns the subtraction of two numbers
	 * 
	 * @param a
	 * @param b
	 * @return subtraction of a and b
	 */
	public int sub(int a, int b) {
		return a - b;
	}

	/**
	 * Returns the multiplication of two numbers
	 * 
	 * @param a
	 * @param b
	 * @return multiplication of a and b
	 */
	public int mult(int a, int b) {
		return a * b;
	}

	/**
	 * Returns the division of two numbers
	 * 
	 * @param a
	 * @param b
	 * @return division of a and b
	 */
	public int div(int a, int b) {
		return a / b;
	}
}

package com.laqr.calculator;
import java.util.Scanner;

/**
 * Calculator Driver
 * @author laqr
 */
public class CalculatorDriver {

	static CalcFunctions functions = new CalcFunctions();

	/**
	 * Main Method
	 * @param args
	 */
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		char opt = 'a';
		do {
			System.out.println("1.Addition\n2.Subtraction\n3.Multiplication\n4.Division");
			System.out.println("Enter operation to be performed");
			int op = sc.nextInt();
			
			System.out.println("Enter first number: ");
			int num1 = sc.nextInt();
			System.out.println("Enter second number: ");
			int num2 = sc.nextInt();
			
			int result;
			
			switch(op) {
				case 1:
					result = functions.add(num1, num2);
					System.out.println("Addition is :"+result);
					break;
				case 2:
					result = functions.sub(num1, num2);
					System.out.println("Subtraction is: " + result);
					break;
				case 3:
					result = functions.mult(num1, num2);
					System.out.println("Multiplication is: " + result);
					break;
				case 4:
					result = functions.div(num1, num2);
					System.out.println("Division is: " + result);
					break;
			}
			
			System.out.println("Do you want to continue?");
			opt = sc.next().charAt(0);
		} while( opt =='y' || opt=='Y' );

		sc.close();
	}
}

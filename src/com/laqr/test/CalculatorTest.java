package com.laqr.test;

import com.laqr.calculator.CalcFunctions;
import junit.framework.TestCase;

/**
 * Calculator Functions Tested ;)
 * @author laqr
 */
public class CalculatorTest extends TestCase {
	
	/**
	 * TestCaseID         : 1
	 * TestDescription    : add two numbers
	 * TestCaseInput      : a = 5, b = 5
	 * TestExpectedOutput : 10
	 */
	public void testAdd001() {
		assertEquals(10, functions.add(5, 5));
	}

	/**
	 * TestCaseID         : 2
	 * TestDescription    : subtract two numbers
	 * TestCaseInput      : a = 20, b = 10
	 * TestExpectedOutput : 10
	 */
	public void testSub001() {
		assertEquals(10, functions.sub(20, 10));
	}

	/**
	 * TestCaseID         : 3
	 * TestDescription    : subtract two numbers
	 * TestCaseInput      : a = 2, b = 10
	 * TestExpectedOutput : 20
	 */
	public void testMult001() {
		assertEquals(20, functions.mult(2, 10));
	}

	/**
	 * TestCaseID         : 4
	 * TestDescription    : division two numbers
	 * TestCaseInput      : a = 10, b = 2
	 * TestExpectedOutput : 5
	 */
	public void testDiv001() {
		assertEquals(5, functions.div(10, 2));
	}

	CalcFunctions functions = new CalcFunctions();
}
